package fr.univangers.ester.EsterWebApp.Controllers;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.univangers.ester.EsterWebApp.Authentication.Authentication;
import fr.univangers.ester.EsterWebApp.Beans.EsterUser;
import fr.univangers.ester.EsterWebApp.Beans.MailServer;
import fr.univangers.ester.EsterWebApp.Beans.Questionnaire;
import fr.univangers.ester.EsterWebApp.Beans.Salarie;
import fr.univangers.ester.EsterWebApp.Beans.UtilisateurBeanInterface;
import fr.univangers.ester.EsterWebApp.Core.SalarieCore;
import fr.univangers.ester.EsterWebApp.Mail.Mail;
import fr.univangers.ester.EsterWebApp.Repositories.EsterUserRepository;
import fr.univangers.ester.EsterWebApp.Repositories.MailServerRepository;
import fr.univangers.ester.EsterWebApp.Repositories.QuestionnaireRepository;
import fr.univangers.ester.EsterWebApp.Repositories.SalarieRepository;
import fr.univangers.ester.EsterWebApp.Security.PwdGenerator;

@Controller
public class EsterUserController {

	public static final String ATT_MSG_WARNING = "Warning";
	public static final String ATT_MSG_SUCCESS = "Success";
	public static final String ATT_MSG_ERROR= "Error";
	public static final String ATT_SESSION_USER = "sessionUtilisateur";
	public static final String ATT_FIRST_CNX = "FirstConnexion";

	@Autowired
	EsterUserRepository esterUserRepository;

	@Autowired
	MailServerRepository mailServerRepository;

	@Autowired
	QuestionnaireRepository questionnaireRepository;

	@Autowired
	SalarieRepository salarieRepository;

	@Autowired
	MailServer mailServer;

	@Autowired
	Authentication authentication;

	@Autowired
	SalarieCore salarieCore;

	// afficherQuestionnaire
	
	@GetMapping("/utilisateur/ajax")
	@ResponseBody
	public List<Questionnaire> getSalarieAnsQuest(HttpServletRequest request){
		/*
		 * Appel pour afficher questionnaire d'un salarie 
		 */
		System.out.println(request.getParameter("salarie_id"));
		List<Questionnaire> salarieAnsQuestionnaire = null;
		if(request.getParameter("salarie_id") != null) {
			String salarieId = request.getParameter("salarie_id");
			Salarie s = salarieRepository.findByAnonymityNumber(salarieId);
			if (s != null) {
				salarieAnsQuestionnaire = s.getAnsweredQuestionnaires();
			}
		}
		return salarieAnsQuestionnaire;
	}
	
	@GetMapping("/utilisateur")
	public String salarieGET(HttpSession session, Model model, HttpServletRequest request){
		UtilisateurBeanInterface sessionUser = (UtilisateurBeanInterface) session.getAttribute(ATT_SESSION_USER);
		if (sessionUser != null && sessionUser.isUtilisateur()) {
			if(sessionUser.isFirstConnection()) {
				model.addAttribute("id", sessionUser.getIdentifiant());
				model.addAttribute(ATT_FIRST_CNX, true);
			}else {
				model.addAttribute(ATT_FIRST_CNX, false);
			}

			if (sessionUser.isAdministrateur()) { // Pour un utilisateur de type Administrateur
				if(request.getParameter("page") != null && request.getParameter("page").equals("configurationServeurMail")) { // Configuration du serveur Mail
					List<MailServer> mailServers = mailServerRepository.findAll();
					if (mailServers.size() == 0) {
						mailServer.setDefaultMailServer();
						mailServerRepository.save(mailServer);
					}else {
						mailServer = mailServers.get(0);
					}
					model.addAttribute("email", mailServer.getMail() );
					model.addAttribute("pass", mailServer.getPassword());
					model.addAttribute("host", mailServer.getHost());
					model.addAttribute("port", mailServer.getSmtpPort());
				}
			}

			if(request.getParameter("page") != null && request.getParameter("page").equals("donnerQuestionnaire")) { //Attribution de questionnaires aux salariés
				session.setAttribute("ListeQuestionnaires", questionnaireRepository.findAll());
				session.setAttribute("ListeSalaries", salarieRepository.findAll());
			}
			
			if(request.getParameter("page") != null && request.getParameter("page").equals("afficherQuestionnaire")) { 
				session.setAttribute("ListeQuestionnaires", null);
				session.setAttribute("ListeSalaries", salarieRepository.findAll());
			}
		}
		return "utilisateur/index";
	}


	@PostMapping("/utilisateur")
	public String postRequest(HttpSession session, HttpServletRequest request, Model model) {
		UtilisateurBeanInterface sessionUser = (UtilisateurBeanInterface)session.getAttribute("sessionUtilisateur");
		if (sessionUser != null && sessionUser.isUtilisateur()) {
			mailServer.setDefaultMailServer();     
			/*
			 * Récupération des éléments dans la page ModifierMotDePasse 
			 * pour le changement de mot de passe
			 */
			if(request.getParameter("page") != null && request.getParameter("page").equals("ModifierMotDePasse")) { 
				String oldPassword = request.getParameter("oldPassword");
				String newPassword = request.getParameter("newPassword");
				String confirm = request.getParameter("confirm");
				if (authentication.modifyPasswordEsterUser(sessionUser.getIdentifiant(), oldPassword, newPassword, confirm)) {
					model.addAttribute("Success", "Mot de passe modifié");
				}
				else {
					model.addAttribute("Warning", "Mot de passe de confirmation ou Ancien Mot de passe incorrect");
				}

			}
			/*
			 * Appel des éléments dans la page donnerQuestionnaire
			 * pour attribuer des questionnaires à des salariés
			 */

			if(request.getParameter("page") != null && request.getParameter("page").equals("donnerQuestionnaire")) {
				String identifiantQuestionnaire = request.getParameter("IdentifiantQuestionnaire");
				String identifiantSalarie = request.getParameter("IdentifiantSalarie");

				try {
					ArrayList<String> listStr = salarieCore.attribuerQuestionnaire(identifiantSalarie, identifiantQuestionnaire);
					session.setAttribute("ListeQuestionnaires", questionnaireRepository.findAll());
					session.setAttribute("ListeSalaries", salarieRepository.findAll());
					model.addAttribute(listStr.get(0), listStr.get(1));
				} catch (Exception e) {
					model.addAttribute(ATT_MSG_ERROR, "Une erreur est survenue lors de l'attribution du questionnaire");
				}
			}
			
			/*
			 * Afficher un questionnaire d'un salarié 
			 */
			
			if(request.getParameter("page") != null && request.getParameter("page").equals("afficherQuestionnaire")) {
				String identifiantQuestionnaire = request.getParameter("IdentifiantQuestionnaire");
				String identifiantSalarie = request.getParameter("IdentifiantSalarie");
				
				//Questionnaire q = questionnaireRepository.findByName(identifiantQuestionnaire);
				Salarie s = salarieRepository.findByAnonymityNumber(identifiantSalarie);
				if (s != null) {
					Questionnaire q = s.askedQuestionnaire(identifiantQuestionnaire);
					if (q != null) {
						model.addAttribute("Identifiant Questionnaire", q.getId());
						model.addAttribute("Nom", q.getName());
						session.setAttribute("qname", q.getName());
						if (q.getSubmissionDate() != null) {
							model.addAttribute("Date", DateFormat.getDateInstance().format(q.getSubmissionDate()));
						}
						model.addAttribute("IdentifiantEster", q.getIdEsterCreator());
						model.addAttribute("Questionnaire", q.getReponsesMap());
						if (q.getAnsweredDate() != null) {
							model.addAttribute("DateReponse", DateFormat.getDateInstance().format(q.getAnsweredDate()));
						}
						return "questionnaires/questionnaireSalarie";
					}
				}
			}
			
			if (sessionUser.isAdministrateur()) { // Concernant l'Administrateur

				/* 
				 * Partie permettant de récupérer les éléments de la page
				 * configurationServeurMail 
				 */
				if(request.getParameter("page") != null && request.getParameter("page").equals("configurationServeurMail")) {
					List<MailServer> mailServers = mailServerRepository.findAll();
					if (mailServers.size() != 0) {
						mailServer = mailServers.get(0);
						mailServer.updateMailServer(request.getParameter("emailSender"), request.getParameter("password"),request.getParameter("host"),request.getParameter("port"));
						mailServerRepository.save(mailServer);
						model.addAttribute("Success", "Serveur mail modifié");
					}else {
						mailServer.updateMailServer(request.getParameter("emailSender"), request.getParameter("password"),request.getParameter("host"),request.getParameter("port"));
						mailServerRepository.save(mailServer);
						model.addAttribute("Success", "Serveur mail modifié");
					}
				}
			}

			/*
			 * Partie permettant de créer un nouveau salarié en appelant une fonction de génération
			 * d'un Identifiant puis stockage dans la Base de données
			 */
			if (sessionUser.isMedecin() || sessionUser.isAdministrateur() || sessionUser.isPreventeur()) {
				if(request.getParameter("page") != null && request.getParameter("page").equals("createSalarie")) {
					String code = PwdGenerator.generateCode();
					model.addAttribute("message",code );
					//ajout à la base
					Salarie s = new Salarie(code, sessionUser.getIdentifiant());
					salarieRepository.save(s);
				}
			}


			if (sessionUser.isMedecin()||sessionUser.isAdministrateur()) {
				String forPath = "/utilisateur";

				/* 
				 * Création d'autres utilisateurs (Préventeur/Assistant/Infirmier voire Médecin)
				 */

				if(request.getParameter("page") != null && request.getParameter("page").equals("createUser")) {
					String email = request.getParameter("email");
					EsterUser esterUser = esterUserRepository.findByMail(email);
					System.out.println("blabla");
					if(esterUser == null) {
						System.out.println("blabla2");
						String type = request.getParameter("typeCompte");
						String pass = PwdGenerator.generatePassword();
						String path = request.getRequestURL().toString();
						path = path.substring(0, path.length()-forPath.length());

						if (authentication.createEsterUser(email, pass, type)) {
							Mail mailSender = new Mail();
							boolean mailSend = mailSender.sendMail(email,"Mot de passe provisoire", mailSender.mdpProvisoireBodyText(pass,path+"/connexion"), true);
							if(mailSend) {
								model.addAttribute(ATT_MSG_SUCCESS,"Compte crée et mail envoyé");
							}
							else {
								model.addAttribute(ATT_MSG_WARNING,"un problème est survenu. Veuillez réessayer plus tard.");
							}
						}else {
							model.addAttribute(ATT_MSG_WARNING,"un problème est survenu. Veuillez réessayer plus tard.");
						}
					}else {
						model.addAttribute(ATT_MSG_ERROR, "Email déjà existant");
					}
				}
			}
		}
		return "utilisateur/index";
	}
}
