package fr.univangers.ester.EsterWebApp.Tests;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import fr.univangers.ester.EsterWebApp.Beans.EsterUser;
import fr.univangers.ester.EsterWebApp.Repositories.EsterUserRepository;

public class EsterUserTest {
	
	
	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	
	private EsterUserRepository esterUserRepository;

	public EsterUserTest(EsterUserRepository pEsterUserRepository) {
		this.esterUserRepository = pEsterUserRepository;
	}

	public void EsterUserRepoTest() {

		esterUserRepository.deleteAll();
		
		System.out.println(bCryptPasswordEncoder.encode("azerty"));
		// $2a$10$38dLiCrboQqMttglTu5G8uBTpK8M8PVYp2pFdNzTsoC1ov4A/jrt6
		// $2a$10$4eGqmhwZVAZD7LWtO8qaa.JTfzFRXJGYGEw7YkmQG78Cc6xOar2eW
		//System.err.println(bCryptPasswordEncoder.matches("azerty", "$2a$10$JIX/6QyIsrXJtZIjhw4EZ.k.9hg7TJJQVGA9D792ZFwGL2XN5QVha"));

		
		EsterUser e = new EsterUser("Nordy", "Guy","aa","ADMINISTRATEUR");
		e.setPassword(bCryptPasswordEncoder.encode("aa"));
		esterUserRepository.save(e);
		esterUserRepository.save(new EsterUser("Nom2", "Prénom2","mail2","MEDECIN"));
		esterUserRepository.save(new EsterUser("Nom3", "Prénom3","mail3","INFIRMIER"));
		esterUserRepository.save(new EsterUser("Nom4", "Prénom4","mail4","ADMINISTRATEUR"));
		esterUserRepository.save(new EsterUser("Nom1", "Prénom1","mail5","ASSISTANT"));

		// fetch all EsterUser
		System.out.println("EsterUser found with findAll():");
		System.out.println("-------------------------------");
		for (EsterUser esterUser : esterUserRepository.findAll()) {
			System.out.println(esterUser);
		}
		System.out.println();

		// fetch an individual EsterUser
		System.out.println("EsterUser found with findByLastName('Nom1'):");
		System.out.println("--------------------------------");
		EsterUser eu1 = esterUserRepository.findFirstByLastName("Nom1");
		EsterUser eu3 = esterUserRepository.findFirstByLastName("Nom3");
		eu1.setUserRole(eu3.getUserRole());
		esterUserRepository.save(eu1);
		System.out.println(eu1);
		
		System.out.println();

		System.out.println("EsterUser found with findByFirstName('Prénom4'):");
		System.out.println("--------------------------------");
		System.out.println(esterUserRepository.findFirstByFirstName("Prénom4"));
		
		System.out.println();
		
		System.out.println("EsterUser found with findByMail('mail3'):");
		System.out.println("--------------------------------");
		System.out.println(esterUserRepository.findByMail("mail3"));
		
		System.out.println();

		System.out.println("EsterUser found with findByUserRole('MEDECIN'):");
		System.out.println("--------------------------------");
		for (EsterUser esterUser : esterUserRepository.findByUserRole("MEDECIN")) {
			System.out.println(esterUser);
		}
	}
}
