package fr.univangers.ester.EsterWebApp.Beans;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

@Component
@Document(collection="C_URL_TOKEN")
public class UrlToken {
	
	@Id
	private String id;
	private String mail;
	private String urlToken;
	private Date expireDate;
	
	public UrlToken() {
		
	}
	
	public void setUrlToken(String mail, String urlToken, Date expireDate) {
		this.mail = mail;
		this.urlToken = urlToken;
		this.expireDate = expireDate;
	}
	
	@Override
	public String toString() {
		return "UrlToken [id="+id+" ,mail=" + mail + ", urlToken=" + urlToken + ", expireDate=" + expireDate + "]";
	}

	
	public String getId() {
		return id;
	}
	

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getUrlToken() {
		return urlToken;
	}

	public void setUrlToken(String urlToken) {
		this.urlToken = urlToken;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}
	
	public boolean isActive() {
		boolean isActive = true;
		Date currentDate = new Date();
		if (currentDate.compareTo(this.expireDate) > 0) {
			isActive = false;
		}
		return isActive;
	}
}
