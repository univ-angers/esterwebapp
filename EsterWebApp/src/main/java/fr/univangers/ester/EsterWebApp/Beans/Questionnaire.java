package fr.univangers.ester.EsterWebApp.Beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="C_QUESTIONNAIRE")
public class Questionnaire {
	
	@Id
	private String id;
	private String name;
	private String htmlQuestionnaire;
	private Map<String, String> reponsesMap = new HashMap<>();
	private ArrayList<Question> questions = new ArrayList<>();
	private int scoreObtenu;
	private int scoreTotalPossible;
	private String idEsterCreator;
	private Date submissionDate;
	private Date answeredDate;
	
	public Questionnaire() {
		this.name = null;
		this.htmlQuestionnaire = null;
		this.questions = null;
		this.scoreObtenu = 0;
		this.scoreTotalPossible = 0;
		this.idEsterCreator = null;
	}
	
	public Questionnaire(String pName) {
		this.name = pName;
		this.scoreObtenu = 0;
		this.scoreTotalPossible = 0;
	}
	
	public Questionnaire(String pName, ArrayList<Question> pQuestions) {
		this.name = pName;
		this.questions = pQuestions;
		calculScoreObtenu();
		calculScoreTotalPossible();
	}
	
	public Questionnaire(String name, String htmlQuestionnaire, String idEsterCreator) {
		this.name = name;
		this.htmlQuestionnaire =  htmlQuestionnaire;
		this.idEsterCreator = idEsterCreator;
		this.submissionDate = new Date();
	}
	
	public void setQuestionnaireHTML(String name, String htmlQuestionnaire, String idEsterCreator) {
		this.name = name;
		this.htmlQuestionnaire =  htmlQuestionnaire;
		this.idEsterCreator = idEsterCreator;
		this.submissionDate = new Date();
	}
	
	@Override
	public String toString() {
		return "Questionnaire [name=" + name + ", questions=" + questions + ", scoreObtenu=" + scoreObtenu
				+ ", scoreTotalPossible=" + scoreTotalPossible +  htmlQuestionnaire + ", "+ submissionDate + "]";
	}
	
	public Map<String, String> getReponsesMap() {
		return reponsesMap;
	}

	public void setReponsesMap(Map<String, String> reponsesMap) {
		this.reponsesMap = reponsesMap;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public Date getAnsweredDate() {
		return answeredDate;
	}

	public void setAnsweredDate(Date answeredDate) {
		this.answeredDate = answeredDate;
	}
	
	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getIdEsterCreator() {
		return idEsterCreator;
	}

	public void setIdEsterCreator(String idEsterCreator) {
		this.idEsterCreator = idEsterCreator;
	}

	// Calcul du score total obtenu a partir de chaque reponse choisie des differentes questions
	public void calculScoreObtenu() {
		this.scoreObtenu = 0;
		for (Question question : questions) {
			this.scoreObtenu += question.getScoreObtenu();
		}
	}
	
	// Calcul du score total possible a partir des reponses des differentes questions
	public void calculScoreTotalPossible() {
		this.scoreTotalPossible = 0;
		for (Question question : questions) {
			this.scoreTotalPossible+=question.getScoreTotalPossible();
		}
	}
	
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHtmlQuestionnaire() {
		return htmlQuestionnaire;
	}

	public void setHtmlQuestionnaire(String htmlQuestionnaire) {
		this.htmlQuestionnaire = htmlQuestionnaire;
	}

	public ArrayList<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(ArrayList<Question> questions) {
		this.questions = questions;
	}

	public int getScoreObtenu() {
		return scoreObtenu;
	}

	public void setScoreObtenu(int scoreObtenu) {
		this.scoreObtenu = scoreObtenu;
	}

	public int getScoreTotalPossible() {
		return scoreTotalPossible;
	}

	public void setScoreTotalPossible(int scoreTotalPossible) {
		this.scoreTotalPossible = scoreTotalPossible;
	}

}
