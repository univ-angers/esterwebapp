package fr.univangers.ester.EsterWebApp.Repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.univangers.ester.EsterWebApp.Beans.Entreprise;

public interface EntrepriseRepository extends MongoRepository<Entreprise, String> {
	
	public Entreprise findByIdentifiant(String identifiant);

}
