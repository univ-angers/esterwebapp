package fr.univangers.ester.EsterWebApp.Tests;

import java.util.ArrayList;

import fr.univangers.ester.EsterWebApp.Beans.Question;
import fr.univangers.ester.EsterWebApp.Beans.Questionnaire;
import fr.univangers.ester.EsterWebApp.Beans.Reponse;
import fr.univangers.ester.EsterWebApp.Repositories.QuestionnaireRepository;

public class QuestionnaireTest {

	private QuestionnaireRepository questionnaireRepository;


	public QuestionnaireTest(QuestionnaireRepository questionnaireRepository) {
		this.questionnaireRepository = questionnaireRepository;
	}

	public void questionnaireRepoTest() {

		questionnaireRepository.deleteAll();

		Reponse r1a = new Reponse("A",1);
		Reponse r1b = new Reponse("B",2);

		Reponse r2a = new Reponse("A",3);
		Reponse r2b = new Reponse("B",4);

		Reponse r3a = new Reponse("A",6);
		Reponse r3b = new Reponse("B",3);

		ArrayList<Reponse> r1list = new ArrayList<>();
		r1list.add(r1a);
		r1list.add(r1b);

		ArrayList<Reponse> r2list = new ArrayList<>();
		r2list.add(r2a);
		r2list.add(r2b);

		ArrayList<Reponse> r3list = new ArrayList<>();
		r3list.add(r3a);
		r3list.add(r3b);

		System.out.println("Création des questions....");

		Question q1 = new Question("un", "q1",r1list);
		q1.setReponseChoisie(q1.getReponses().get(0));
		q1.calculScoreObtenu();
		q1.calculScoreTotalPossible();

		Question q2 = new Question("deux", "q2",r2list);
		q2.setReponseChoisie(q2.getReponses().get(1));
		q2.calculScoreObtenu();
		q2.calculScoreTotalPossible();

		Question q3 = new Question("trois", "q3",r3list);
		q3.setReponseChoisie(q3.getReponses().get(0));
		q3.calculScoreObtenu();
		q3.calculScoreTotalPossible();


		System.out.println("Création du questionnaire....");
		Questionnaire survey = new Questionnaire("Questionnaire1");
		survey.getQuestions().add(q1);
		survey.getQuestions().add(q2);
		survey.getQuestions().add(q3);
		survey.calculScoreObtenu();
		survey.calculScoreTotalPossible();
		System.out.println("Questionnaire : ");
		System.out.println("Nom : " + survey.getName());
		System.out.println("Nombre de questions : " + survey.getQuestions().size());
		System.out.println(survey.getScoreObtenu()+"/"+survey.getScoreTotalPossible());
		
		
		System.out.println("/////////");
		System.out.println("Traitement avec la base de données");
		
		questionnaireRepository.save(survey);

		// fetch all questions
		System.out.println("Questionnaires found with findAll():");
		System.out.println("-------------------------------");
		for (Questionnaire questionnaire : questionnaireRepository.findAll()) {
			System.out.println(questionnaire);
		}
		System.out.println();

		// fetch an individual question
		System.out.println("Questionnaire found with findByQuestionId('qid2'):");
		System.out.println("--------------------------------");
		System.out.println(questionnaireRepository.findByName("Questionnaire1"));

		System.out.println();

		System.out.println("Questionnaires found with findByScoreGreaterThan(10):");
		System.out.println("--------------------------------");
		for (Questionnaire questionnaire : questionnaireRepository.findByScoreObtenuGreaterThan(10)) {
			System.out.println(questionnaire);
		}

	}

}
