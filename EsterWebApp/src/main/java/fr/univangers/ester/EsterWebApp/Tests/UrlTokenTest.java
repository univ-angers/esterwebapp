package fr.univangers.ester.EsterWebApp.Tests;

import java.util.Calendar;
import java.util.Date;

import fr.univangers.ester.EsterWebApp.Beans.UrlToken;
import fr.univangers.ester.EsterWebApp.Repositories.UrlTokenRepository;

public class UrlTokenTest {
	
private UrlTokenRepository urlTokenRepository;
	
	public UrlTokenTest(UrlTokenRepository urlTokenRepository) {
		this.urlTokenRepository = urlTokenRepository;
	}

	public void UrlTokenRepTest() {

		urlTokenRepository.deleteAll();
		// save a couple of salarie
		
		UrlToken urlToken = new UrlToken();
		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.MINUTE, 4);
		dt = c.getTime();
		urlToken.setUrlToken("aa@aa.fr", "aaaa", dt);
		urlTokenRepository.save(urlToken);
		
		System.out.println(urlTokenRepository.findByMail("aa@aa.fr"));
		
	}

}
