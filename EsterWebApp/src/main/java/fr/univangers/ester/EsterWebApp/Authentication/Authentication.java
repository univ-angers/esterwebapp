package fr.univangers.ester.EsterWebApp.Authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import fr.univangers.ester.EsterWebApp.Beans.Entreprise;
import fr.univangers.ester.EsterWebApp.Beans.EsterUser;
import fr.univangers.ester.EsterWebApp.Beans.Salarie;
import fr.univangers.ester.EsterWebApp.Repositories.EntrepriseRepository;
import fr.univangers.ester.EsterWebApp.Repositories.EsterUserRepository;
import fr.univangers.ester.EsterWebApp.Repositories.SalarieRepository;

@Component
public class Authentication {

	@Autowired
	EsterUserRepository esterUserRepository;

	@Autowired
	SalarieRepository salarieRepository;

	@Autowired
	EntrepriseRepository entrepriseRepository;


	BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();


	public EsterUser authenticateEsterUser(String mail, String password) {
		EsterUser esterUser = esterUserRepository.findByMail(mail);
		if (esterUser != null) {
			if (esterUser.getMail() != null && esterUser.getPassword() != null) {
				if (bCryptPasswordEncoder.matches(password, esterUser.getPassword())) {
					return esterUser;
				}
			}
		}
		return null;
	}


	public Salarie authenticateSalarie(String anonymityNumber) {
		Salarie salarie = salarieRepository.findByAnonymityNumber(anonymityNumber);
		if (salarie != null) {
			if (salarie.getAnonymityNumber() != null) {
				if (salarie.getAnonymityNumber().equals(anonymityNumber)){
					return salarie;
				}
			}
		}
		return null;
	}


	public Entreprise authenticateEntreprise(String id, String password) {
		Entreprise entreprise = entrepriseRepository.findByIdentifiant(id);
		if (entreprise != null) {
			if (entreprise.getPassword() != null) {
				if (bCryptPasswordEncoder.matches(password, entreprise.getPassword())) {
					return entreprise;
				}
			}
		}
		return null;
	}
	
	
	public boolean esterUserExists(String mail) {
		boolean exists = false;
		EsterUser esterUser = esterUserRepository.findByMail(mail);
		if (esterUser != null) {
			exists = true;
		}
		return exists;
	}
	
	
	public boolean entrepriseExists(String mail) {
		boolean exists = false;
		Entreprise entreprise = entrepriseRepository.findByIdentifiant(mail);
		if (entreprise != null) {
			exists = true;
		}
		return exists;
	}
	
	
	public boolean changePasswordEntreprise(String mail, String password) {
		boolean ok = false;
		Entreprise entreprise = entrepriseRepository.findByIdentifiant(mail);
		if (entreprise != null) {
			entreprise.setPassword(bCryptPasswordEncoder.encode(password));
			entrepriseRepository.save(entreprise);
			ok = true;
		}else {
			ok = false;
		}
		return ok;
	}
	
	
	public boolean changePasswordEsterUser(String mail, String password) {
		boolean ok = false;
		EsterUser esterUser = esterUserRepository.findByMail(mail);
		if (esterUser != null) {
			esterUser.setPassword(bCryptPasswordEncoder.encode(password));
			esterUserRepository.save(esterUser);
			ok = true;
		}else {
			ok = false;
		}
		return ok;
	}
	
	public boolean modifyPasswordEsterUser(String esterUserMail, String oldPassword, String newPassword, String passwordConfirmation) {
		boolean ok = false;
		EsterUser esterUser = esterUserRepository.findByMail(esterUserMail);
		if (esterUser != null) {
			if (bCryptPasswordEncoder.matches(oldPassword, esterUser.getPassword())) {
				if (newPassword.equals(passwordConfirmation)) {
					esterUser.setPassword(bCryptPasswordEncoder.encode(newPassword));
					esterUserRepository.save(esterUser);
					ok = true;
				}
			}
		}
		return ok;
	}
	
	public boolean createEsterUser(String mail, String password, String status) {
		boolean ok = false;
		if (esterUserRepository.findByMail(mail)==null) {
			EsterUser esterUser = new EsterUser(mail, bCryptPasswordEncoder.encode(password), status);
			esterUserRepository.save(esterUser);
			ok = true;
		}
		return ok;
	}
}
