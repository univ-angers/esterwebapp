package fr.univangers.ester.EsterWebApp.Repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.univangers.ester.EsterWebApp.Beans.EsterUser;

public interface EsterUserRepository extends MongoRepository<EsterUser, String> {
	
	public EsterUser findFirstByLastName(String lastName);
	public EsterUser findFirstByFirstName(String firstName);
	public EsterUser findByMail(String mail);
    public List<EsterUser> findByUserRole(String userRole);
}
