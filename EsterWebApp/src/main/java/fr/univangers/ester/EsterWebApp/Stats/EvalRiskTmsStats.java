package fr.univangers.ester.EsterWebApp.Stats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.univangers.ester.EsterWebApp.Beans.Questionnaire;
import fr.univangers.ester.EsterWebApp.Beans.Salarie;
import fr.univangers.ester.EsterWebApp.Repositories.SalarieRepository;

public class EvalRiskTmsStats {
	
	/* TODO
	 * Récupération de tous les questionnaires EvalRisk de tous les salariés
	 * Calculer le pourcentage de chaque questionnaire
	 * Etablir un graphique pour le score total (abscisse de 0 à ScoreTotalPossible du questionnaire par pas de 1)
	 * Récupération des toutes les questions EvalRisk
	 * Etablir un graphique pour chaque question
	*/
	
	SalarieRepository salarieRepository;
	
	ArrayList<Questionnaire> evalriskquestionnaires = new ArrayList<>();
	
	HashMap<Integer,Double> map = new HashMap<>();
	
	public EvalRiskTmsStats( SalarieRepository salarieRepository ) {
		this.salarieRepository = salarieRepository;
		getEvalRiskTms();
		calculPourcentageQuestionnaire();
		//displayMap();
	}
	
	private void getEvalRiskTms() {
		List<Salarie> salaries = salarieRepository.findAll();
		for (Salarie s : salaries) {
			for (Questionnaire q : s.getAnsweredQuestionnaires()) {
				if (q.getName().equals("Eval-Risk-TMS")) {
					evalriskquestionnaires.add(q);
				}
			}
		}
	}
	
	// --------------- Global Score ----------------------
	private void calculPourcentageQuestionnaire() {
		int total = evalriskquestionnaires.size();
		double one = 1;
		for (Questionnaire q : evalriskquestionnaires) {
			if (map.get(q.getScoreObtenu()) == null) {
				map.put(q.getScoreObtenu(), (one/total)*100);
			}else {
				map.put(q.getScoreObtenu(), map.get(q.getScoreObtenu()) + ((one/total)*100));
			}
		}
	}
	
	public ArrayList<Double> data(){
		ArrayList<Double> data = new ArrayList<>();
		for (int i = 0; i < evalriskquestionnaires.get(0).getScoreTotalPossible()+1; i++) {
			if (map.get(i) == null) {
				data.add(0.0);
			}else {
				data.add(Math.ceil(map.get(i)));
			}
		}
		return data;
	}
	
	public ArrayList<String> globalCategories(){
		ArrayList<String> globalC = new ArrayList<>();
		for (int i = 0; i < evalriskquestionnaires.get(0).getScoreTotalPossible()+1 ; i++) {
			globalC.add(String.valueOf(i));
		}
		return globalC;
	}

	public void displayMap() {
		Iterator it = map.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        System.out.println(pair.getKey() + " = " + pair.getValue());
	        it.remove(); // avoids a ConcurrentModificationException
	    }
	}
	// -----------------------------------------------------------------
	
	// ------------------------ Questions --------------------------------
	
//	public ArrayList<String> getCategories_2() {
//		ArrayList<String> categories = new ArrayList<>();
//		categories.add("Votre travail nécessite-t-il de répéter les mêmes actions plus de 2 à 4 fois environ par minute ?");
//		categories.add("Travaillez-vous avec un ou deux bras en l'air (au-dessus des épaules) régulièrement ou de manière prolongée ?");
//		categories.add("Fléchir le(s) coude(s) régulièrement ou de manière prolongée ?");
//		categories.add("Presser ou prendre fermement des objets ou des pièces entre le pouce et l'index ?");
//		System.out.println(categories.toString());
//		return categories;
//	}
//	
//	public ArrayList<Integer> getSerieOne() {
//		int idtmsQ2 = 0;
//		int idtmsQ2Total = 0;
//		int idtmsQ4 = 0;
//		int idtmsQ4Total = 0;
//		int idtmsQ5 = 0;
//		int idtmsQ5Total = 0;
//		int idtmsQ6 = 0;
//		int idtmsQ6Total = 0;
//		ArrayList<Integer> serieToujours = new ArrayList<>();
//		for (Questionnaire questionnaire : evalriskquestionnaires) {
//			for (Question question : questionnaire.getQuestions()) {
//				if (question.getId().equals("IdtmsQ2")) {
//					idtmsQ2Total++;
//					if (question.getReponseChoisie().getIntitule().contains("Toujours") || question.getReponseChoisie().getIntitule().contains("La plupart")) {
//						idtmsQ2++;
//					}
//				}
//				if (question.getId().equals("IdtmsQ4")) {
//					idtmsQ4Total++;
//					if (question.getReponseChoisie().getIntitule().contains("Toujours") || question.getReponseChoisie().getIntitule().contains("La plupart")) {
//						idtmsQ4++;
//					}
//				}
//				if (question.getId().equals("IdtmsQ5")) {
//					idtmsQ5Total++;
//					if (question.getReponseChoisie().getIntitule().contains("Toujours") || question.getReponseChoisie().getIntitule().contains("La plupart")) {
//						idtmsQ5++;
//					}
//				}
//				if (question.getId().equals("IdtmsQ6")) {
//					idtmsQ6Total++;
//					if (question.getReponseChoisie().getIntitule().contains("Toujours") || question.getReponseChoisie().getIntitule().contains("La plupart")) {
//						idtmsQ6++;
//					}
//				}
//			}
//		}
//		serieToujours.add(idtmsQ2);
//		serieToujours.add(idtmsQ4);
//		serieToujours.add(idtmsQ5);
//		serieToujours.add(idtmsQ6);
//		
//		System.out.println(serieToujours.toString());
//		return serieToujours;
//	}
}
