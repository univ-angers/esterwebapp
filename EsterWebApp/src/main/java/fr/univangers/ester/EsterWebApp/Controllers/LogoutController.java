package fr.univangers.ester.EsterWebApp.Controllers;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LogoutController {
	
	@GetMapping("/deconnexion")
	public String deconnexionGET(HttpSession session) {
		session.invalidate();
		return "redirect:/connexion";
	}

}
