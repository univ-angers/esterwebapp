package fr.univangers.ester.EsterWebApp.Tests;

import java.util.ArrayList;

import fr.univangers.ester.EsterWebApp.Beans.Question;
import fr.univangers.ester.EsterWebApp.Beans.Questionnaire;
import fr.univangers.ester.EsterWebApp.Beans.Reponse;
import fr.univangers.ester.EsterWebApp.Repositories.QuestionRepository;
import fr.univangers.ester.EsterWebApp.Repositories.QuestionnaireRepository;

public class EvalRiskTmsTest {

	QuestionnaireRepository questionnaireRepository;

	QuestionRepository questionRepository;
	
	public EvalRiskTmsTest(QuestionnaireRepository questionnaireRepository, QuestionRepository questionRepository) {
		this.questionnaireRepository = questionnaireRepository;
		this.questionRepository = questionRepository;
	}

	public void evalRiskTMSTest() {

		questionnaireRepository.deleteAll();
		questionRepository.deleteAll();

		// Création de chaque question
		// q1 et réponses
		Reponse q1R1 = new Reponse("<15", 0);
		Reponse q1R2 = new Reponse(">15", 2);
		ArrayList<Reponse> alq1 = new ArrayList<>();
		alq1.add(q1R1);
		alq1.add(q1R2);
		Question tmsQ1 = new Question("IdtmsqQ1", "Comment évaluez-vous l'intensite ́des efforts physiques de votre travail au cours d'une journée de travail?", alq1);
//		tmsQ1.setReponseChoisie(q1R1);
//		tmsQ1.calculScoreObtenu();
//		tmsQ1.calculScoreTotalPossible();

		// q2 et réponses
		Reponse q2R1 = new Reponse("Non ou presque jamais", 0);
		Reponse q2R2 = new Reponse("Souvent (de 2 à 4h/jour", 0);
		Reponse q2R3 = new Reponse("Rarement (moins de 2h/jour", 0);
		Reponse q2R4 = new Reponse("Toujours ou presque toujours (>4h/jour)", 2);
		ArrayList<Reponse> alq2 = new ArrayList<>();
		alq2.add(q2R1);
		alq2.add(q2R2);
		alq2.add(q2R3);
		alq2.add(q2R4);
		Question tmsQ2 = new Question("IdtmsQ2","Votre travail nécessite-t-il de répéter les mêmes actions plus de 2 à 4 fois environ par minute?",alq2);
//		tmsQ2.setReponseChoisie(q2R4);
//		tmsQ2.calculScoreObtenu();
//		tmsQ2.calculScoreTotalPossible();


		// q3 et réponses
		Reponse q3R1 = new Reponse("Jamais", 0);
		Reponse q3R2 = new Reponse("Souvent (de 2 à 4h/jour", 2);
		Reponse q3R3 = new Reponse("Rarement (moins de 2h/jour", 0);
		Reponse q3R4 = new Reponse("Toujours (>4h/jour)", 2);
		ArrayList<Reponse> alq3 = new ArrayList<>();
		alq3.add(q3R1);
		alq3.add(q3R2);
		alq3.add(q3R3);
		alq3.add(q3R4);
		Question tmsQ3 = new Question("IdtmsQ3","Devez-vous pencher en avant / sur le côté régulièrement ou de manière prolongée ?",alq3);
//		tmsQ3.setReponseChoisie(q3R4);
//		tmsQ3.calculScoreObtenu();
//		tmsQ3.calculScoreTotalPossible();

		// q4 et réponses
		Reponse q4R1 = new Reponse("Jamais ou presque jamais", 0);
		Reponse q4R2 = new Reponse("Souvent (de 2 à 4h/jour", 2);
		Reponse q4R3 = new Reponse("Rarement (moins de 2h/jour", 0);
		Reponse q4R4 = new Reponse("La plupart du temps (>4h/jour)", 2);
		ArrayList<Reponse> alq4 = new ArrayList<>();
		alq4.add(q4R1);
		alq4.add(q4R2);
		alq4.add(q4R3);
		alq4.add(q4R4);
		Question tmsQ4 = new Question("IdtmsQ4","Travaillez-vous avec un ou deux bras en l'air (au-dessus des épaules) régulièrement ou de manière prolongée ?",alq4);
//		tmsQ4.setReponseChoisie(q4R4);
//		tmsQ4.calculScoreObtenu();
//		tmsQ4.calculScoreTotalPossible();

		// q5 et réponses
		Reponse q5R1 = new Reponse("Jamais ou presque jamais", 0);
		Reponse q5R2 = new Reponse("Souvent (de 2 à 4h/jour", 1);
		Reponse q5R3 = new Reponse("Rarement (moins de 2h/jour", 0);
		Reponse q5R4 = new Reponse("La plupart du temps (>4h/jour)", 1);
		ArrayList<Reponse> alq5 = new ArrayList<>();
		alq5.add(q5R1);
		alq5.add(q5R2);
		alq5.add(q5R3);
		alq5.add(q5R4);
		Question tmsQ5 = new Question("IdtmsQ5","Fléchir le(s) coude(s) régulièrement ou de manière prolongée ?",alq5);
//		tmsQ5.setReponseChoisie(q5R4);
//		tmsQ5.calculScoreObtenu();
//		tmsQ5.calculScoreTotalPossible();

		// q6 et réponses
		Reponse q6R1 = new Reponse("Jamais ou presque jamais", 0);
		Reponse q6R2 = new Reponse("Souvent (de 2 à 4h/jour", 0);
		Reponse q6R3 = new Reponse("Rarement (moins de 2h/jour", 0);
		Reponse q6R4 = new Reponse("La plupart du temps (>4h/jour)", 2);
		ArrayList<Reponse> alq6 = new ArrayList<>();
		alq6.add(q6R1);
		alq6.add(q6R2);
		alq6.add(q6R3);
		alq6.add(q6R4);
		Question tmsQ6 = new Question("IdtmsQ6","Presser ou prendre fermement des objets ou des pièces entre le pouce et l'index ?",alq6);
//		tmsQ6.setReponseChoisie(q6R4);
//		tmsQ6.calculScoreObtenu();
//		tmsQ6.calculScoreTotalPossible();

		// q7 et réponses
		Reponse q7R1 = new Reponse("Pas du tout d'accord", 2);
		Reponse q7R2 = new Reponse("Pas d'accord", 2);
		Reponse q7R3 = new Reponse("D'accord", 0);
		Reponse q7R4 = new Reponse("Tout à fait d'accord", 0);
		ArrayList<Reponse> alq7 = new ArrayList<>();
		alq7.add(q7R1);
		alq7.add(q7R2);
		alq7.add(q7R3);
		alq7.add(q7R4);
		Question tmsQ7 = new Question("IdtmsQ7","Dans mon travail j'ai la possibilité d'influencer le déroulement de mon travail ?",alq7);
//		tmsQ7.setReponseChoisie(q7R4);
//		tmsQ7.calculScoreObtenu();
//		tmsQ7.calculScoreTotalPossible();

		// q8 et réponses
		Reponse q8R1 = new Reponse("Pas du tout d'accord", 2);
		Reponse q8R2 = new Reponse("Pas d'accord", 2);
		Reponse q8R3 = new Reponse("D'accord", 0);
		Reponse q8R4 = new Reponse("Tout à fait d'accord", 0);
		ArrayList<Reponse> alq8 = new ArrayList<>();
		alq8.add(q8R1);
		alq8.add(q8R2);
		alq8.add(q8R3);
		alq8.add(q8R4);
		Question tmsQ8 = new Question("IdtmsQ8","Les collègues avec qui je travaille m'aident à mener les tâches à bien ?",alq8);
//		tmsQ8.setReponseChoisie(q8R2);
//		tmsQ8.calculScoreObtenu();
//		tmsQ8.calculScoreTotalPossible();
		
		// Toutes les questions
		ArrayList<Question> evalRiskTmsQuestions = new ArrayList<>();
		evalRiskTmsQuestions.add(tmsQ1);
		evalRiskTmsQuestions.add(tmsQ2);
		evalRiskTmsQuestions.add(tmsQ3);
		evalRiskTmsQuestions.add(tmsQ4);
		evalRiskTmsQuestions.add(tmsQ5);
		evalRiskTmsQuestions.add(tmsQ6);
		evalRiskTmsQuestions.add(tmsQ7);
		evalRiskTmsQuestions.add(tmsQ8);

		Questionnaire evalRiskTms = new Questionnaire("Eval-Risk-TMS",evalRiskTmsQuestions);
		evalRiskTms.calculScoreObtenu();
		evalRiskTms.calculScoreTotalPossible();
		for (Question question : evalRiskTmsQuestions) {
			questionRepository.save(question);
		}
		
		questionnaireRepository.save(evalRiskTms);
		
		System.out.println("Questionnaire : ");
		System.out.println("Nom : " + evalRiskTms.getName());
	}

}
