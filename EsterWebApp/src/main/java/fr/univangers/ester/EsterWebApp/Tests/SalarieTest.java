package fr.univangers.ester.EsterWebApp.Tests;

import java.util.ArrayList;

import fr.univangers.ester.EsterWebApp.Beans.Question;
import fr.univangers.ester.EsterWebApp.Beans.Questionnaire;
import fr.univangers.ester.EsterWebApp.Beans.Reponse;
import fr.univangers.ester.EsterWebApp.Beans.Salarie;
import fr.univangers.ester.EsterWebApp.Repositories.SalarieRepository;

public class SalarieTest {

	private SalarieRepository salarieRepository;
	
	public SalarieTest(SalarieRepository pSalarieRepository) {
		this.salarieRepository = pSalarieRepository;
	}

	public void salarieRepoTest() {

		salarieRepository.deleteAll();
		// save a couple of salarie
		
		salarieRepository.save(new Salarie("azerty1", "idEU1"));
		salarieRepository.save(new Salarie("azerty2", "idEU1"));
		salarieRepository.save(new Salarie("azerty3", "idEU1"));
		salarieRepository.save(new Salarie("azerty4", "idEU1"));
		
		//Add unansweredquestionnaire
		Salarie s = new Salarie("azert", "idEU1");
		
		s.addAnsweredQuestionnaires(unQuestionnaire());
		salarieRepository.save(s);
		

		// fetch all salarie
		System.out.println("Salarie found with findAll():");
		System.out.println("-------------------------------");
		for (Salarie salarie : salarieRepository.findAll()) {
			System.out.println(salarie);
		}
		System.out.println();

		// fetch an individual salarie
		System.out.println("Salarie found with findByAnonymityNumber('azerty2'):");
		System.out.println("--------------------------------");
		System.out.println(salarieRepository.findByAnonymityNumber("azerty2"));

		System.out.println("Salarie found with findBySexe('femme'):");
		System.out.println("--------------------------------");
		for (Salarie salarie : salarieRepository.findBySexe("femme")) {
			System.out.println(salarie);
		}
	}
	
	private Questionnaire unQuestionnaire() {
		
		Reponse r1a = new Reponse("A",1);
		Reponse r1b = new Reponse("B",2);

		Reponse r2a = new Reponse("A",3);
		Reponse r2b = new Reponse("B",4);

		Reponse r3a = new Reponse("A",6);
		Reponse r3b = new Reponse("B",3);

		ArrayList<Reponse> r1list = new ArrayList<>();
		r1list.add(r1a);
		r1list.add(r1b);

		ArrayList<Reponse> r2list = new ArrayList<>();
		r2list.add(r2a);
		r2list.add(r2b);

		ArrayList<Reponse> r3list = new ArrayList<>();
		r3list.add(r3a);
		r3list.add(r3b);

		System.out.println("Création des questions....");

		Question q1 = new Question("un", "q1",r1list);
		q1.setReponseChoisie(q1.getReponses().get(0));
		q1.calculScoreObtenu();
		q1.calculScoreTotalPossible();

		Question q2 = new Question("deux", "q2",r2list);
		q2.setReponseChoisie(q2.getReponses().get(1));
		q2.calculScoreObtenu();
		q2.calculScoreTotalPossible();

		Question q3 = new Question("trois", "q3",r3list);
		q3.setReponseChoisie(q3.getReponses().get(0));
		q3.calculScoreObtenu();
		q3.calculScoreTotalPossible();


		System.out.println("Création du questionnaire....");
		Questionnaire survey = new Questionnaire("Questionnaire1");
		survey.getQuestions().add(q1);
		survey.getQuestions().add(q2);
		survey.getQuestions().add(q3);
		survey.calculScoreObtenu();
		survey.calculScoreTotalPossible();
		
		return survey;
	}
	
	public void salarieAnswersASurvey() {
		
		Salarie azert = salarieRepository.findByAnonymityNumber("azert");
		Questionnaire evalrisk = azert.getUnansweredQuestionnaires().get(0);
		
		for (Question q : evalrisk.getQuestions()) {
			q.setReponseChoisie(q.getReponses().get(1));
			q.calculScoreObtenu();
			q.calculScoreTotalPossible();
		}
		
		evalrisk.calculScoreObtenu();
		evalrisk.calculScoreTotalPossible();
		
		azert.addAnsweredQuestionnaires(evalrisk);
		azert.getUnansweredQuestionnaires().remove(evalrisk);
		
		salarieRepository.save(azert);
	}
}
