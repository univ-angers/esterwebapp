package fr.univangers.ester.EsterWebApp.Controllers;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import fr.univangers.ester.EsterWebApp.Beans.Questionnaire;
import fr.univangers.ester.EsterWebApp.Beans.Salarie;
import fr.univangers.ester.EsterWebApp.Beans.UtilisateurBeanInterface;
import fr.univangers.ester.EsterWebApp.Repositories.QuestionnaireRepository;
import fr.univangers.ester.EsterWebApp.Repositories.SalarieRepository;



@Controller
public class QuestionnaireController {
	
	public static final String ATT_SOURCE  = "source";
    public static final String ATT_IDENTIFIANT = "Identifiant";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    public static final String ATT_NOM = "Nom";
    public static final String ATT_MSG_WARNING = "Warning";
    public static final String ATT_MSG_SUCCESS = "Success";
    
    @Autowired
	QuestionnaireRepository questionnaireRepository;
    
    @Autowired
	SalarieRepository salarieRepository;
    
    
    @GetMapping("/salarie/questionnaire")
	public String questionnaireSalarieGET(HttpSession session, Model model, HttpServletRequest request){
		Salarie s = (Salarie) session.getAttribute(ATT_SESSION_USER);
		List<Questionnaire> allQuestionnaires = new ArrayList<>();
		List<Questionnaire> unAnsQ = s.getUnansweredQuestionnaires();
		List<Questionnaire> ansQ = s.getAnsweredQuestionnaires();
		allQuestionnaires.addAll(unAnsQ);
		allQuestionnaires.addAll(ansQ);
		model.addAttribute("ListeQuestionnaires",allQuestionnaires);
		return "questionnaires/Questionnaire";
	}
	
	@PostMapping("/salarie/questionnaire")
	public String questionnaireSalariePOST(HttpSession session, Model model, HttpServletRequest request){
		Salarie s = (Salarie) session.getAttribute(ATT_SESSION_USER);
		String identifiantQ = request.getParameter("Identifiant");
		if (s.askedQuestionnaire(identifiantQ) != null) {
			Questionnaire q = s.askedQuestionnaire(identifiantQ);
			model.addAttribute("Identifiant Questionnaire", q.getId());
			model.addAttribute("Nom", q.getName());
			session.setAttribute("qname", q.getName());
			if (q.getSubmissionDate() != null) {
				model.addAttribute("Date", DateFormat.getDateInstance().format(q.getSubmissionDate()));
			}
			model.addAttribute("IdentifiantEster", q.getIdEsterCreator());
			model.addAttribute("Questionnaire", q.getHtmlQuestionnaire());
			if (q.getAnsweredDate() != null) {
				model.addAttribute("DateReponse", DateFormat.getDateInstance().format(q.getAnsweredDate()));
			}
		}
		return "questionnaires/Questionnaire";
	}
	
	@PostMapping("/salarie/questionnaireSubmit")
	public String questionnaireSalarieSubmitPOST(HttpSession session, Model model, HttpServletRequest request){
		Salarie s = (Salarie) session.getAttribute(ATT_SESSION_USER);
		String identifiantQ = (String) session.getAttribute("qname");
		Enumeration<String> names = request.getParameterNames();
        Map<String, String> reponses = new HashMap<>();
        // Récupere tous les reponses envoyer
		while (names.hasMoreElements()) {
            String identifiantQuestion = names.nextElement();
            String reponse = request.getParameter(identifiantQuestion);
            reponses.put(identifiantQuestion, reponse);
    	} 
		
		Questionnaire q = s.askedQuestionnaire(identifiantQ);
		if (q != null) {
			//String qid = q.getId();
			Questionnaire newq = q;
			s.getUnansweredQuestionnaires().remove(q);
			newq.setReponsesMap(reponses);
			newq.setAnsweredDate(new Date());
			s.addAnsweredQuestionnaires(newq);
			salarieRepository.save(s);
		}
//		System.out.println(s.getAnonymityNumber());
//		System.out.println("ID Q :" + identifiantQ);
//		Iterator it = reponses.entrySet().iterator();
//	    while (it.hasNext()) {
//	        Map.Entry pair = (Map.Entry)it.next();
//	        System.out.println(pair.getKey() + " = " + pair.getValue());
//	        it.remove(); // avoids a ConcurrentModificationException
//	    }
		
		
		return "redirect:/salarie/questionnaire";
	}
    
    
    @GetMapping("/utilisateur/questionnaire")
	public String questionnaireGET(HttpSession session,Model model) {
		List<Questionnaire> qlist = questionnaireRepository.findAll();
		model.addAttribute("ListeQuestionnaires",qlist);
		return "questionnaires/Questionnaire";
	}
    
    @PostMapping("/utilisateur/questionnaire")
	public String questionnaireEsterPOST(HttpSession session, Model model, HttpServletRequest request){
		String identifiantQ = request.getParameter("Identifiant");
		Questionnaire q = questionnaireRepository.findByName(identifiantQ);
		
		if (q != null) {
			model.addAttribute("Identifiant Questionnaire", q.getId());
			model.addAttribute("Nom", q.getName());
			model.addAttribute("Date", DateFormat.getDateInstance().format(q.getSubmissionDate()));
			model.addAttribute("IdentifiantEster", q.getIdEsterCreator());
			model.addAttribute("Questionnaire", q.getHtmlQuestionnaire());
		}
		
		return "questionnaires/Questionnaire";
	}

    
	@GetMapping("/utilisateur/generateur_questionnaire")
	public String generateurDeQuestionGET(HttpSession session,Model model) {
		return "questionnaires/generateur_de_question";
	}
	
	
	@PostMapping("/utilisateur/generateur_questionnaire")
	public String generateurDeQuestionPOST(HttpSession session, HttpServletRequest request, Model model) {
		String source = request.getParameter(ATT_SOURCE);
		
   		if(source.length() != 0) {
   			String nom = request.getParameter(ATT_NOM);
   			String identifiantEster = ((UtilisateurBeanInterface)session.getAttribute(ATT_SESSION_USER)).getIdentifiant();
   			Questionnaire questionnaire = questionnaireRepository.findByName(nom);
   			if(questionnaire != null) {
			   model.addAttribute(ATT_MSG_WARNING, "Identifiant deja utilisé");
   			} else {
			   model.addAttribute(ATT_MSG_SUCCESS, "Questionnaire sauvegarder");
			   Questionnaire q = new Questionnaire(nom, source, identifiantEster);
			   questionnaireRepository.save(q);
   			}
       }
		return "questionnaires/generateur_de_question";
	}
	
}
