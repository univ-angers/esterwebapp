package fr.univangers.ester.EsterWebApp.Controllers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import fr.univangers.ester.EsterWebApp.Authentication.Authentication;
import fr.univangers.ester.EsterWebApp.Beans.UrlToken;
import fr.univangers.ester.EsterWebApp.Mail.Mail;
import fr.univangers.ester.EsterWebApp.Repositories.UrlTokenRepository;

@Controller
public class PasswordController {

	public static final String ATT_MSG_WARNING = "Warning";
	public static final String ATT_MSG_SUCCESS = "Success";
	private  String email;

	@Autowired
	Authentication authentication;

	@Autowired
	UrlToken urlToken;

	@Autowired
	UrlTokenRepository urlTokenRepository;

	@GetMapping("/ForgotPassword")
	public String forgotPasswordGET(HttpSession session,Model model) {
		return "motDePasse/ForgotPassword";
	}

	@PostMapping("/ForgotPassword")
	public String forgotPasswordPOST(HttpServletRequest request, Model model) {
		PasswordController.TimeLimit delai = new TimeLimit(0, 0, 3, 0);
		email = request.getParameter("Email");
		String path = request.getRequestURL().toString();
		path = path.substring(0, path.length()-"ForgotPassword".length());
		Mail mailSender = new Mail();

		if (authentication.entrepriseExists(email) || authentication.esterUserExists(email) ) {
			if(urlTokenRepository.findByMail(email) != null) {
				model.addAttribute(ATT_MSG_WARNING, "Un mail valide vous a déjà été envoyé.");
				return "motDePasse/ForgotPassword";
			}else {
				String token = UUID.randomUUID().toString();
				Date expireDate = delai.addTimeLimitToDate(new Date());

				urlToken.setUrlToken(email, token, expireDate);

				URL urlTokenUrl = null;
				try {
					urlTokenUrl = new URL(path+"ResetPassword?"+"token="+token);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
				//send email
				System.out.println("envoi mail...");
				boolean mailSend = mailSender.sendMail(email,"Demande de réinitialisation de mot de passe", mailSender.mdpOublieBodyText(urlTokenUrl.toString()), true);
				if(mailSend) {
					urlTokenRepository.save(urlToken);
					model.addAttribute(ATT_MSG_SUCCESS,"mail envoyé");
					return "motDePasse/ForgotPassword";
				}else {
					model.addAttribute(ATT_MSG_WARNING,"un problème est survenu. Veuillez réessayer plus tard.");
					//if message not sent delete token in database
					urlTokenRepository.deleteById(urlToken.getId());
					return "motDePasse/ForgotPassword";
				}
			}
		}else {
			model.addAttribute(ATT_MSG_WARNING, "Email n'existe pas");
			return "motDePasse/ForgotPassword";
		}
	}

	public class TimeLimit{
		int nbJours;
		int nbHeures;
		int nbMinutes;
		int nbSecondes;

		public TimeLimit(int nbJours,int nbHeures,int nbMinutes,int nbSecondes) {
			this.nbJours=nbJours;
			this.nbHeures=nbHeures;
			this.nbMinutes=nbMinutes;
			this.nbSecondes=nbSecondes;
		}

		public Date addTimeLimitToDate(Date date) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DATE, this.nbJours);
			calendar.add(Calendar.HOUR_OF_DAY, this.nbHeures);
			calendar.add(Calendar.MINUTE, this.nbMinutes);
			calendar.add(Calendar.SECOND, this.nbSecondes);
			return calendar.getTime();
		}

	}

	@GetMapping("/ResetPassword")
	public String resetPasswordGET(HttpServletRequest request,Model model) {
		//get token 
		String token = request.getParameter("token");

		//check if valid
		urlToken = urlTokenRepository.findByUrlToken(token);
		if (urlToken != null && urlToken.isActive()) {
			request.setAttribute("message", null);
			email = urlToken.getMail();
			request.setAttribute("id", email);
			request.setAttribute("valid", true);
			return "motDePasse/ResetPassword";
		}
		
		request.setAttribute("message",null);
		return "motDePasse/ResetPassword";
	}
	
	
	@PostMapping("/ResetPassword")
	public String resetPasswordPOST(HttpServletRequest request,Model model) {
		String pwd = request.getParameter("password");	
		
		if (authentication.entrepriseExists(email)) {
			if (authentication.changePasswordEntreprise(email, pwd)) {
				urlToken = urlTokenRepository.findByMail(email);
				urlTokenRepository.deleteById(urlToken.getId());
				model.addAttribute(ATT_MSG_SUCCESS, "Mot de passe modifié.");
			}else {
				model.addAttribute(ATT_MSG_WARNING, "Un problème est survenu.Veuillez réessayer plus tard.");
			}
		}else if(authentication.esterUserExists(email)){
			if (authentication.changePasswordEsterUser(email, pwd)) {
				urlToken = urlTokenRepository.findByMail(email);
				urlTokenRepository.deleteById(urlToken.getId());
				model.addAttribute(ATT_MSG_SUCCESS, "Mot de passe modifié.");
			}else {
				model.addAttribute(ATT_MSG_WARNING, "Un problème est survenu.Veuillez réessayer plus tard.");
			}
		}else {
			model.addAttribute(ATT_MSG_WARNING, "Un problème est survenu.Veuillez réessayer plus tard.");
		}
		model.addAttribute("message", true);
		return "motDePasse/ResetPassword";
	}
}