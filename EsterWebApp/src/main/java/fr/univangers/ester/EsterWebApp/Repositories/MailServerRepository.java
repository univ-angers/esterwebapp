package fr.univangers.ester.EsterWebApp.Repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.univangers.ester.EsterWebApp.Beans.MailServer;

public interface MailServerRepository extends MongoRepository<MailServer, String> {
	
	public MailServer findByMail(String mail);

}
