package fr.univangers.ester.EsterWebApp.Tests;

import java.util.ArrayList;

import fr.univangers.ester.EsterWebApp.Beans.Question;
import fr.univangers.ester.EsterWebApp.Beans.Reponse;
import fr.univangers.ester.EsterWebApp.Repositories.QuestionRepository;

public class QuestionTest {

	private QuestionRepository questionRepository;
	
	
	public QuestionTest(QuestionRepository questionRepository) {
		this.questionRepository = questionRepository;
	}
	
	public void questionRepoTest() {

		questionRepository.deleteAll();

		Reponse r1a = new Reponse("A",1);
		Reponse r1b = new Reponse("B",2);

		Reponse r2a = new Reponse("A",3);
		Reponse r2b = new Reponse("B",4);

		Reponse r3a = new Reponse("A",6);
		Reponse r3b = new Reponse("B",3);

		ArrayList<Reponse> r1list = new ArrayList<>();
		r1list.add(r1a);
		r1list.add(r1b);

		ArrayList<Reponse> r2list = new ArrayList<>();
		r2list.add(r2a);
		r2list.add(r2b);

		ArrayList<Reponse> r3list = new ArrayList<>();
		r3list.add(r3a);
		r3list.add(r3b);


		System.out.println("Création des questions....");

		Question q1 = new Question("qid1","q1",r1list);
		q1.setReponseChoisie(q1.getReponses().get(0));
		q1.calculScoreObtenu();
		q1.calculScoreTotalPossible();

		Question q2 = new Question("qid2","q2",r2list);
		q2.setReponseChoisie(q2.getReponses().get(1));
		q2.calculScoreObtenu();
		q2.calculScoreTotalPossible();

		Question q3 = new Question("qid3","q3",r3list);
		q3.setReponseChoisie(q3.getReponses().get(0));
		q3.calculScoreObtenu();
		q3.calculScoreTotalPossible();

		System.out.println(q1.getScoreObtenu()+"/"+q1.getScoreTotalPossible());
		System.out.println(q2.getScoreObtenu()+"/"+q2.getScoreTotalPossible());
		System.out.println(q3.getScoreObtenu()+"/"+q3.getScoreTotalPossible());

		System.out.println("/////////");
		System.out.println("Traitement avec la base de données");

		questionRepository.save(q1);
		questionRepository.save(q2);
		questionRepository.save(q3);

		// fetch all questions
		System.out.println("Questions found with findAll():");
		System.out.println("-------------------------------");
		for (Question question : questionRepository.findAll()) {
			System.out.println(question);
		}
		System.out.println();

		// fetch an individual question
		System.out.println("Question found with findByQuestionId('qid2'):");
		System.out.println("--------------------------------");
		System.out.println(questionRepository.findByQuestionId("qid2"));

		System.out.println();

		System.out.println("Questions found with findByScoreGreaterThan(4):");
		System.out.println("--------------------------------");
		for (Question question : questionRepository.findByScoreObtenuGreaterThan(3)) {
			System.out.println(question);
		}
	}
}
