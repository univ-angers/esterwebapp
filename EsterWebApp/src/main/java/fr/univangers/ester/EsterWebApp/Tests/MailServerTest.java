package fr.univangers.ester.EsterWebApp.Tests;

import fr.univangers.ester.EsterWebApp.Beans.MailServer;
import fr.univangers.ester.EsterWebApp.Repositories.MailServerRepository;

public class MailServerTest {
	
	private MailServerRepository mailServerRepository;

	public MailServerTest(MailServerRepository mailServerRepository) {
		this.mailServerRepository = mailServerRepository;
	}

	public void mailServerRepoTest() {

		mailServerRepository.deleteAll();
		
		mailServerRepository.save(new MailServer());
		
		MailServer mailServer = mailServerRepository.findByMail("projet.ester@gmail.com");
		System.out.println(mailServer);
		
	}

}
