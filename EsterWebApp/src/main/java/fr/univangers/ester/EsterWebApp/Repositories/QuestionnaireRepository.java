package fr.univangers.ester.EsterWebApp.Repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.univangers.ester.EsterWebApp.Beans.Questionnaire;

public interface QuestionnaireRepository extends MongoRepository<Questionnaire, String> {
	
	public Questionnaire findByName(String name);
    public List<Questionnaire> findByScoreObtenuGreaterThan(int score);

}
