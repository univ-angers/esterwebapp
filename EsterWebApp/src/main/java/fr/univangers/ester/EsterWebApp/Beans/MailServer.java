package fr.univangers.ester.EsterWebApp.Beans;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

@Component
@Document(collection="C_MAIL_SERVER")
public class MailServer {
	
	@Id
	private String id;
	private String mail;
	private String password;
	private String smtpHost;
	private String smtpPort;
	
	public MailServer() {
		
	}
	
	public MailServer(String mail, String password, String host, String smtpPort) {
		super();
		this.mail = mail;
		this.password = password;
		this.smtpHost = host;
		this.smtpPort = smtpPort;
	}
	
	public void setDefaultMailServer() {
		this.mail = "noreply.mail.ester@gmail.com";
		this.password = "Est3r@dmin";
		this.smtpHost = "smtp.gmail.com";	
		this.smtpPort = "587";
	}
	
	public void updateMailServer(String mail, String password, String host, String smtpPort) {
		this.mail = mail;
		this.password = password;
		this.smtpHost = host;
		this.smtpPort = smtpPort;
	}
	
	public String getId() {
		return this.id;
	}
	
	@Override
	public String toString() {
		return "MailServer [mail=" + mail + ", password=" + password + ", smtpHost=" + smtpHost + ", smtpPort="
				+ smtpPort + "]";
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getHost() {
		return smtpHost;
	}


	public void setHost(String host) {
		this.smtpHost = host;
	}


	public String getSmtpPort() {
		return smtpPort;
	}


	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}
	
}
