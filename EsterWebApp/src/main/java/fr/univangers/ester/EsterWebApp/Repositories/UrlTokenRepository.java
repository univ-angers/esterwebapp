package fr.univangers.ester.EsterWebApp.Repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.univangers.ester.EsterWebApp.Beans.UrlToken;

public interface UrlTokenRepository extends MongoRepository<UrlToken, String> {

	public UrlToken findByMail(String mail); //mail de la personne
	public UrlToken findByExpireDate(String expireDate);
	public UrlToken findByUrlToken(String urlToken);
	
}
