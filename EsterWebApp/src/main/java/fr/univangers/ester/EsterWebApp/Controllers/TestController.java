package fr.univangers.ester.EsterWebApp.Controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.univangers.ester.EsterWebApp.Repositories.EntrepriseRepository;
import fr.univangers.ester.EsterWebApp.Repositories.EsterUserRepository;
import fr.univangers.ester.EsterWebApp.Repositories.MailServerRepository;
import fr.univangers.ester.EsterWebApp.Repositories.QuestionRepository;
import fr.univangers.ester.EsterWebApp.Repositories.QuestionnaireRepository;
import fr.univangers.ester.EsterWebApp.Repositories.SalarieRepository;
import fr.univangers.ester.EsterWebApp.Repositories.UrlTokenRepository;
import fr.univangers.ester.EsterWebApp.Stats.EvalRiskTmsStats;
import fr.univangers.ester.EsterWebApp.Tests.EntrepriseTest;
import fr.univangers.ester.EsterWebApp.Tests.EsterUserTest;
import fr.univangers.ester.EsterWebApp.Tests.EvalRiskTmsTest;
import fr.univangers.ester.EsterWebApp.Tests.MailServerTest;
import fr.univangers.ester.EsterWebApp.Tests.QuestionTest;
import fr.univangers.ester.EsterWebApp.Tests.QuestionnaireTest;
import fr.univangers.ester.EsterWebApp.Tests.SalarieTest;
import fr.univangers.ester.EsterWebApp.Tests.UrlTokenTest;

@Controller
public class TestController {

	@Autowired
	SalarieRepository salarieRepository;

	@Autowired
	QuestionRepository questionRepository;

	@Autowired 
	QuestionnaireRepository questionnaireRepository;

	@Autowired
	EsterUserRepository esterUserRepository;

	@Autowired
	EntrepriseRepository entrepriseRepository;

	@Autowired
	MailServerRepository mailServerRepository;

	@Autowired
	UrlTokenRepository urlTokenRepository;


	@GetMapping("/utilisateur/questionnaire_eval")
	public String qEval(HttpSession session,Model model) {
		//UtilisateurBeanInterface utilisateur = new EsterUser("nom","prenom","mail","ASSISTANT");
		//session.setAttribute("sessionUtilisateur", utilisateur);
		//model.addAttribute("Warning", "Message de warning");
		//model.addAttribute("valid",true);
		return "questionnaires/questionnaire_eval";
	}


	@GetMapping("/entreprise")
	public String entreprise(HttpSession session,Model model) {
		//		UtilisateurBeanInterface utilisateur = new EsterUser("nom","prenom","mail","ASSISTANT");
		//		session.setAttribute("sessionUtilisateur", utilisateur);
		//model.addAttribute("Warning", "Message de warning");
		return "entreprise/index";
	}
	
	@GetMapping("/resultat")
	public String resultat(HttpSession session,Model model) {
		//		UtilisateurBeanInterface utilisateur = new EsterUser("nom","prenom","mail","ASSISTANT");
		//		session.setAttribute("sessionUtilisateur", utilisateur);
		//model.addAttribute("Warning", "Message de warning");
		EvalRiskTmsStats evalrisktmsstats = new EvalRiskTmsStats(salarieRepository);
		model.addAttribute("data", evalrisktmsstats.data());
		model.addAttribute("globalCategories",evalrisktmsstats.globalCategories());
//		model.addAttribute("categories_2",evalrisktmsstats.getCategories_2());
//		model.addAttribute("serieOne",evalrisktmsstats.getSerieOne());

		return "resultats/resultat";
	}
	
	
	
/*
 * Controller pour toutes les classes de test
 * présent dans le package Tests
 */
	
	@GetMapping("/evalrisktmstest")
	@ResponseBody
	public String evalrisktmstest() {
		EvalRiskTmsTest evalrtms = new EvalRiskTmsTest(questionnaireRepository,questionRepository);
		evalrtms.evalRiskTMSTest();
		return "Test sur EvalRiskTMS";
	}
	
	
	
	@GetMapping("/urltokentest")
	@ResponseBody
	public String urlTokenTest() {

		UrlTokenTest url = new UrlTokenTest(urlTokenRepository);
		url.UrlTokenRepTest();
		return "Test sur urlToken";
	}

	@GetMapping("/mailservertest")
	@ResponseBody
	public String mailServerTest() {
		// Lancement des Tests pour ServerMail
		MailServerTest mailServerTest = new MailServerTest(mailServerRepository);
		mailServerTest.mailServerRepoTest();
		return "Test sur le serveur mail";
	}

	@GetMapping("/salarietest")
	@ResponseBody
	public String salarietest() {

		// Lancement des Tests pour Salarie
		SalarieTest salarieTest = new SalarieTest(salarieRepository);
		//salarieTest.salarieRepoTest();
		salarieTest.salarieAnswersASurvey();

		return "Tests sur salariés";
	}

	@GetMapping("/questiontest")
	@ResponseBody
	public String questiontest() {

		// Lancement des Tests pour Question
		QuestionTest questionTest = new QuestionTest(questionRepository);
		questionTest.questionRepoTest();

		return "Tests sur questions";
	}

	@GetMapping("/questionnairetest")
	@ResponseBody
	public String questionnairetest() {

		// Lancement des Tests pour Question
		QuestionnaireTest questionnaireTest = new QuestionnaireTest(questionnaireRepository);
		questionnaireTest.questionnaireRepoTest();

		return "Tests sur questionnaires";
	}

	@GetMapping("/esterusertest")
	@ResponseBody
	public String esterusertest() {

		// Lancement des Tests pour Question
		EsterUserTest esterUserTest = new EsterUserTest(esterUserRepository);
		esterUserTest.EsterUserRepoTest();

		return "Tests sur esteruser";
	}

	@GetMapping("/entreprisetest")
	@ResponseBody
	public String entreprisetest() {

		// Lancement des Tests pour Question
		EntrepriseTest entrepriseTest = new EntrepriseTest(entrepriseRepository);
		entrepriseTest.entrepriseRepoTest();

		return "Tests sur esteruser";
	}
}