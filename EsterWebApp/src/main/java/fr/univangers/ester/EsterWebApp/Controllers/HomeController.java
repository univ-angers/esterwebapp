package fr.univangers.ester.EsterWebApp.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String homeGET() {
		return "home/index";
	}
}
