package fr.univangers.ester.EsterWebApp.Beans;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="C_QUESTION")
public class Question {
	
	@Id
	private String id;
	
	private String questionId;
	private String intitule;
	private String htmlQuestion;
	private ArrayList<Reponse> reponses = new ArrayList<>();
	private Reponse reponseChoisie;
	private int scoreObtenu;
	private int scoreTotalPossible;
	
	public Question() {
		this.intitule = null;
		this.htmlQuestion = null;
		this.reponseChoisie = null;
		this.reponses = null;
		this.scoreObtenu = 0;
		this.scoreTotalPossible = 0;
	}
	
	public Question(String pQuestionId,String pIntitule, ArrayList<Reponse> pReponses) {
		this.questionId = pQuestionId;
		this.intitule = pIntitule;
		this.reponses = pReponses;
		this.scoreObtenu = 0;
		this.scoreTotalPossible = 0;
	}
	
	@Override
	public String toString() {
		return "Question [questionId=" + questionId + ", intitule=" + intitule + ", reponses=" + reponses
				+ ", reponseChoisie=" + reponseChoisie.toString() + ", scoreObtenu=" + scoreObtenu + ", scoreTotalPossible="
				+ scoreTotalPossible + "]";
	}

	// Recuperation du score de la reponse choisie
	public void calculScoreObtenu() {
		this.scoreObtenu = this.reponseChoisie.getScore();
	}
	
	// Calcul du score total possible a partir des reponses composant la question
	public void calculScoreTotalPossible() {
		this.scoreTotalPossible = 0;
		for (Reponse reponse : reponses) {
			if (this.scoreTotalPossible < reponse.getScore()) {
				this.scoreTotalPossible = reponse.getScore();
			}
		}
	}

	public String getId() {
		return id;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getHtmlQuestion() {
		return htmlQuestion;
	}

	public void setHtmlQuestion(String htmlQuestion) {
		this.htmlQuestion = htmlQuestion;
	}

	public ArrayList<Reponse> getReponses() {
		return reponses;
	}

	public void setReponses(ArrayList<Reponse> reponses) {
		this.reponses = reponses;
	}

	public Reponse getReponseChoisie() {
		return reponseChoisie;
	}

	public void setReponseChoisie(Reponse reponseChoisie) {
		this.reponseChoisie = reponseChoisie;
	}

	public int getScoreObtenu() {
		return scoreObtenu;
	}

	public void setScoreObtenu(int scoreObtenu) {
		this.scoreObtenu = scoreObtenu;
	}

	public int getScoreTotalPossible() {
		return scoreTotalPossible;
	}

	public void setScoreTotalPossible(int scoreTotalPossible) {
		this.scoreTotalPossible = scoreTotalPossible;
	}	
}
