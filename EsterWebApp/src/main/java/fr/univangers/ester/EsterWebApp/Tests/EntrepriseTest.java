package fr.univangers.ester.EsterWebApp.Tests;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import fr.univangers.ester.EsterWebApp.Beans.Entreprise;
import fr.univangers.ester.EsterWebApp.Repositories.EntrepriseRepository;

public class EntrepriseTest {
	
	private EntrepriseRepository entrepriseRepository;
	
	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	public EntrepriseTest(EntrepriseRepository pEntrepriseRepository) {
		this.entrepriseRepository = pEntrepriseRepository;
	}

	public void entrepriseRepoTest() {
	
		entrepriseRepository.deleteAll();
		
		entrepriseRepository.save(new Entreprise("nordinaryguy@gmail.com",bCryptPasswordEncoder.encode("pass")));
		
		System.out.println(entrepriseRepository.findByIdentifiant("nordinaryguy@gmail.com"));
		
	}

}
