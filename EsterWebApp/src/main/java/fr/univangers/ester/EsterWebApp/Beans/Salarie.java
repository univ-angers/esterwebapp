package fr.univangers.ester.EsterWebApp.Beans;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="C_SALARIE")
public class Salarie implements UtilisateurBeanInterface {
	
	@Id 
	private String id;
	
	private String anonymityNumber;
	
	private boolean firstConnection;
	private String sexe;
	private String yearOfBirthRange;
	private String department;
	private String businessSector;
	private String pCS;
	private int ConnectionsNumber;
	private ArrayList<Questionnaire> answeredQuestionnaires = new ArrayList<>();
	private ArrayList<Questionnaire> unansweredQuestionnaires = new ArrayList<>();
	private String userEsterCreator;
	private String userRole = "SALARIE";
	
	public Salarie (String anonymityNumber, String userEsterCreator) {
		this.anonymityNumber = anonymityNumber;
		this.userEsterCreator = userEsterCreator;
		this.firstConnection = true;
		
	}

	@Override
	public String toString() {
		return "Salarie [anonymityNumber=" + anonymityNumber + ", firstConnection=" + firstConnection
				+ ", sexe=" + sexe + ", yearOfBirthRange=" + yearOfBirthRange + ", department=" + department
				+ ", businessSector=" + businessSector + ", pCS=" + pCS + ", ConnectionsNumber=" + ConnectionsNumber
				+ ", UserRole : "+userRole+"]";
	}
	
	
	public Questionnaire askedQuestionnaire(String name) {
		Questionnaire q = new Questionnaire();
		for (Questionnaire questionnaire : answeredQuestionnaires) {
			if (questionnaire.getName().equals(name)) {
				q = questionnaire;
				return q;
			}
		}
		for (Questionnaire questionnaire : unansweredQuestionnaires) {
			if (questionnaire.getName().equals(name)) {
				q = questionnaire;
				return q;
			}
		}
		return q;
	}
	
	public String getId() {
		return this.id;
	}

	public String getAnonymityNumber() {
		return anonymityNumber;
	}

	public void setAnonymityNumber(String anonymityNumber) {
		this.anonymityNumber = anonymityNumber;
	}

	public boolean isFirstConnection() {
		return firstConnection;
	}

	public void setFirstConnection(boolean firstConnection) {
		this.firstConnection = firstConnection;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getYearOfBirthRange() {
		return yearOfBirthRange;
	}

	public void setYearOfBirthRange(String yearOfBirthRange) {
		this.yearOfBirthRange = yearOfBirthRange;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getBusinessSector() {
		return businessSector;
	}

	public void setBusinessSector(String businessSector) {
		this.businessSector = businessSector;
	}

	public String getpCS() {
		return pCS;
	}

	public void setpCS(String pCS) {
		this.pCS = pCS;
	}

	public int getConnectionsNumber() {
		return ConnectionsNumber;
	}

	public void setConnectionsNumber(int connectionsNumber) {
		ConnectionsNumber = connectionsNumber;
	}

	public ArrayList<Questionnaire> getAnsweredQuestionnaires() {
		return answeredQuestionnaires;
	}

	public void setAnsweredQuestionnaires(ArrayList<Questionnaire> answeredQuestionnaires) {
		this.answeredQuestionnaires = answeredQuestionnaires;
	}

	public ArrayList<Questionnaire> getUnansweredQuestionnaires() {
		return unansweredQuestionnaires;
	}

	public void setUnansweredQuestionnaires(ArrayList<Questionnaire> unansweredQuestionnaires) {
		this.unansweredQuestionnaires = unansweredQuestionnaires;
	}

	
	public String getUserEsterCreator() {
		return userEsterCreator;
	}

	public void setUserEsterCreator(String userEsterCreator) {
		this.userEsterCreator = userEsterCreator;
	}

	public String getUserRole() {
		return userRole;
	}

	public void addAnsweredQuestionnaires(Questionnaire pQuestionnaire) {
		this.answeredQuestionnaires.add(pQuestionnaire);
	}
	
	public void addUnAnsweredQuestionnaires(Questionnaire pQuestionnaire) {
		this.unansweredQuestionnaires.add(pQuestionnaire);
	}

	@Override
	public void setIdentifiant(String identifiant) {
		this.anonymityNumber = identifiant;
	}

	@Override
	public String getIdentifiant() {
		return this.anonymityNumber;
	}

	@Override
	public boolean validate() {
		// TODO connexion à l'appli
		return false;
	}

	@Override
	public boolean isEntreprise() {
		return false;
	}

	@Override
	public boolean isSalarie() {
		return true;
	}

	@Override
	public boolean isUtilisateur() {
		return false;
	}

	@Override
	public boolean isAdministrateur() {
		return false;
	}

	@Override
	public boolean isMedecin() {
		return false;
	}

	@Override
	public boolean isPreventeur() {
		return false;
	}

	@Override
	public boolean isAssistant() {
		return false;
	}

	@Override
	public boolean isInfirmier() {
		return false;
	}
	
	public void updateSalarieProfile(String sexe, String yearOfbirth, String departement, String naf, String pcs) {
		this.sexe = sexe;
		this.yearOfBirthRange = yearOfbirth;
		this.department = departement;
		this.businessSector = naf;
		this.pCS = pcs;
	}
	
}
