package fr.univangers.ester.EsterWebApp.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {
	
	@GetMapping("/entravaux")
	public String enTravauxGET() {
		return "errors/EnDev.html";
	}

}
