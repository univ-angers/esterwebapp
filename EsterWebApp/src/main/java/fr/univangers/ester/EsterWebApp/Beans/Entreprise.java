package fr.univangers.ester.EsterWebApp.Beans;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="C_ENTREPRISE")
public class Entreprise implements UtilisateurBeanInterface {

	@Id
	private String id;
	private String identifiant;
	private String password;

	
	public Entreprise(String identifiant, String password) {
		super();
		this.identifiant = identifiant;
		this.password = password;
	}
	
	public String getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Entreprise [identifiant=" + identifiant + ", password=" + password + "]";
	}

	@Override
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	@Override
	public String getIdentifiant() {
		return identifiant;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}

	@Override
	public boolean validate() {
		//TODO
		return false;
	}

	@Override
	public boolean isEntreprise() {
		return true;
	}

	@Override
	public boolean isSalarie() {
		return false;
	}

	@Override
	public boolean isUtilisateur() {
		return false;
	}

	@Override
	public boolean isAdministrateur() {
		return false;
	}

	@Override
	public boolean isMedecin() {
		return false;
	}

	@Override
	public boolean isPreventeur() {
		return false;
	}

	@Override
	public boolean isAssistant() {
		return false;
	}

	@Override
	public boolean isInfirmier() {
		return false;
	}

	@Override
	public boolean isFirstConnection() {
		return false;
	}

}
