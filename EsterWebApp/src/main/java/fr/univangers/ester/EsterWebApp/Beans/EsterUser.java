package fr.univangers.ester.EsterWebApp.Beans;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="C_ESTER_USER")
public class EsterUser implements UtilisateurBeanInterface{

	@Id
	private String id;

	private String lastName;
	private String firstName;
	private String idCountGeneration;
	private String mail;
	private boolean firstConnection;
	private String password;
	private String userRole;
	private ArrayList<String> accessRights;

	public EsterUser() {
		id = null;
		lastName = null;
		firstName= null;
		idCountGeneration= null;
		mail= null;
		firstConnection=false;
		password= null;
		userRole= null;
		accessRights= null;
	}
	
	public EsterUser(String mail, String password, String userRole) {
		this.mail= mail;
		firstConnection=true;
		this.password = password;
		this.userRole = userRole;
		lastName = null;
		firstName= null;
		idCountGeneration= null;
		accessRights= null;
	}

	public EsterUser(String pLastName, String pFirstName, String pMail, String pUserRole ) {
		id = null;
		lastName = pLastName;
		firstName= pFirstName;
		idCountGeneration= null;
		mail= pMail;
		firstConnection=false;
		password= null;
		userRole = pUserRole;
		accessRights= null;
	}

	public EsterUser(String pLastName, String pFirstName, String pIdCountGeneration, String pMail, String pPassword, String pUserRole, ArrayList<String> pAccessRights) {
		id = null;
		lastName = pLastName;
		firstName= pFirstName;
		idCountGeneration= pIdCountGeneration;
		mail= pMail;
		firstConnection=true;
		password= pPassword;
		userRole = pUserRole;
		accessRights= pAccessRights;
	}

	@Override
	public String toString() {
		return "EsterUser [id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", mail=" + mail
				+ ", userRole=" + userRole + "]";
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIdCountGeneration() {
		return idCountGeneration;
	}

	public void setIdCountGeneration(String idCountGeneration) {
		this.idCountGeneration = idCountGeneration;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isFirstConnection() {
		return firstConnection;
	}

	public void setFirstConnection(boolean firstConnection) {
		this.firstConnection = firstConnection;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public ArrayList<String> getAccessRights() {
		return accessRights;
	}

	public void setAccessRights(ArrayList<String> accessRights) {
		this.accessRights = accessRights;
	}

	public String getId() {
		return id;
	}

	@Override
	public void setIdentifiant(String identifiant) {
		this.mail = identifiant;
	}

	@Override
	public String getIdentifiant() {
		return this.mail;
	}

	@Override
	public boolean validate() {
		// TODO connexion a l'appli
		return false;
	}

	@Override
	public boolean isEntreprise() {
		return false;
	}

	@Override
	public boolean isSalarie() {
		return false;
	}

	@Override
	public boolean isUtilisateur() {
		return true;
	}

	@Override
	public boolean isAdministrateur() {
		return userRole.equals("ADMINISTRATEUR");
	}

	@Override
	public boolean isMedecin() {
		return userRole.equals("MEDECIN");
	}

	@Override
	public boolean isPreventeur() {
		return userRole.equals("PREVENTEUR");
	}

	@Override
	public boolean isAssistant() {
		return userRole.equals("ASSISTANT");
	}

	@Override
	public boolean isInfirmier() {
		return userRole.equals("INFIRMIER");
	}
	
}
