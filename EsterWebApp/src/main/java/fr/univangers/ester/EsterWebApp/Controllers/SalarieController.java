package fr.univangers.ester.EsterWebApp.Controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import fr.univangers.ester.EsterWebApp.Beans.Salarie;
import fr.univangers.ester.EsterWebApp.Beans.UtilisateurBeanInterface;
import fr.univangers.ester.EsterWebApp.Repositories.SalarieRepository;

@Controller
public class SalarieController {

	public static final String ATT_MSG_WARNING = "Warning";
	public static final String ATT_MSG_SUCCESS = "Success";
	public static final String ATT_SESSION_USER = "sessionUtilisateur";
	public static final String ATT_FIRST_CNX = "FirstConnexion";

	@Autowired
	SalarieRepository salarieRepository;

	@GetMapping("/salarie")
	public String salarieGET(HttpSession session, Model model, HttpServletRequest request){
		UtilisateurBeanInterface sessionUser = (UtilisateurBeanInterface) session.getAttribute(ATT_SESSION_USER);
		if (sessionUser != null && sessionUser.isSalarie()) {
			if(sessionUser.isFirstConnection() ||  (request.getParameter("page")!= null && request.getParameter("page").equals("modifierProfil"))){
				model.addAttribute(ATT_FIRST_CNX, true);
			}else {
				model.addAttribute(ATT_FIRST_CNX, false);
			}
		}
		return "salarie/index";
	}


	@PostMapping("/salarie")
	public String salariePOST(HttpSession session, Model model, HttpServletRequest request){
		UtilisateurBeanInterface sessionUser = (UtilisateurBeanInterface) session.getAttribute(ATT_SESSION_USER);
		model.addAttribute(ATT_FIRST_CNX, false);
		if(sessionUser.isFirstConnection() || (request.getParameter("page")!= null && request.getParameter("page").equals("modifierProfil"))){
			Salarie salarie = salarieRepository.findByAnonymityNumber(sessionUser.getIdentifiant());
			String sexe;
			String birthYear = request.getParameter("years");
			String pcs = request.getParameter("pcs");
			String naf = request.getParameter("naf");
			String departement = request.getParameter("departement");

			if (sessionUser.isFirstConnection()) {
				sexe = request.getParameter("sexe");
			}else {
				sexe = salarie.getSexe();
			}

			try {
				salarie.updateSalarieProfile(sexe, birthYear, departement, naf, pcs);
				salarie.setFirstConnection(false);
				salarieRepository.save(salarie);
				model.addAttribute(ATT_MSG_SUCCESS,"Profil mis à jour ");
			} catch (Exception e) {
				model.addAttribute(ATT_MSG_WARNING,"un problème est survenu. Veuillez réessayer plus tard.");
			}
		}
		return "salarie/index";
	}
}
