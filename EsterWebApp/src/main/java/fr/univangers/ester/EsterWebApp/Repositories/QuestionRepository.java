package fr.univangers.ester.EsterWebApp.Repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.univangers.ester.EsterWebApp.Beans.Question;

public interface QuestionRepository extends MongoRepository<Question, String> {
	
	public Question findByQuestionId(String questionId);
    public List<Question> findByScoreObtenuGreaterThan(int score);

}
