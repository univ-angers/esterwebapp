package fr.univangers.ester.EsterWebApp.Beans;


public class Reponse {
	
	private String intitule;
	private String htmlReponse;
	private int score;

	public Reponse() {
		this.intitule = null;
		this.htmlReponse = null;
		this.score = 0;
	}
	
	public Reponse(String pIntitule, int pScore) {
		this.intitule = pIntitule;
		this.score = pScore;
	}	
	
	public Reponse(String pIntitule, int pScore, String pHtmlReponse) {
		this.intitule = pIntitule;
		this.score = pScore;
		this.htmlReponse = pHtmlReponse;
	}

	@Override
	public String toString() {
		return "Reponse [intitule=" + intitule + ", score=" + score + "]";
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getHtmlReponse() {
		return htmlReponse;
	}

	public void setHtmlReponse(String htmlReponse) {
		this.htmlReponse = htmlReponse;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
