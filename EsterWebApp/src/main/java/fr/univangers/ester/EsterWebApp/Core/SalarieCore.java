package fr.univangers.ester.EsterWebApp.Core;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.univangers.ester.EsterWebApp.Beans.Questionnaire;
import fr.univangers.ester.EsterWebApp.Beans.Salarie;
import fr.univangers.ester.EsterWebApp.Repositories.QuestionnaireRepository;
import fr.univangers.ester.EsterWebApp.Repositories.SalarieRepository;

@Component
public class SalarieCore {

	@Autowired
	SalarieRepository salarieRepository;

	@Autowired
	QuestionnaireRepository questionnaireRepository;

	Salarie salarie;
	Questionnaire questionnaire;

	public ArrayList<String> attribuerQuestionnaire(String anonymityNumber, String idQuestionnaire) {
		ArrayList<String> listStr = new ArrayList<>();
		listStr.add("Warning");
		listStr.add("Error : no employee or survey object");
		salarie = salarieRepository.findByAnonymityNumber(anonymityNumber);
		questionnaire = questionnaireRepository.findByName(idQuestionnaire);
		if (salarie != null && questionnaire != null) {
			if (!questionnaireInUnansweredQuestionnaires(salarie, questionnaire)) {
				if (!questionnaireInAnsweredQuestionnaires(salarie, questionnaire)) {
					salarie.addUnAnsweredQuestionnaires(questionnaire);
					salarieRepository.save(salarie);
					listStr.add(0, "Success");
					listStr.add(1, "Questionnaire ajouté");
				}else {
					listStr.add(0, "Warning");
					listStr.add(1, "Questionnaire déjà répondu par le salarié");
				}
			}else {
				listStr.add(0, "Warning");
				listStr.add(1, "Questionnaire déjà attribué au salarié");
			}
		}
		return listStr;
	}

	private boolean questionnaireInUnansweredQuestionnaires(Salarie ps, Questionnaire pq) {
		boolean ok = false;
		for (Questionnaire q : ps.getUnansweredQuestionnaires()) {
			if (q.getName().equals(pq.getName())) {
				return true;
			}
		}
		return ok;
	}

	private boolean questionnaireInAnsweredQuestionnaires(Salarie ps, Questionnaire pq) {
		boolean ok = false;
		for (Questionnaire q : ps.getAnsweredQuestionnaires()) {
			if (q.getName().equals(pq.getName())) {
				return true;
			}
		}
		return ok;
	}

}
