package fr.univangers.ester.EsterWebApp.Repositories;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import fr.univangers.ester.EsterWebApp.Beans.Salarie;

public interface SalarieRepository extends MongoRepository<Salarie,String> {
	
	public Salarie findByAnonymityNumber(String anonymityNumber);
    public List<Salarie> findBySexe(String sexe);
}
