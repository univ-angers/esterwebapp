package fr.univangers.ester.EsterWebApp.Controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;

import fr.univangers.ester.EsterWebApp.Authentication.Authentication;
import fr.univangers.ester.EsterWebApp.Beans.Entreprise;
import fr.univangers.ester.EsterWebApp.Beans.EsterUser;
import fr.univangers.ester.EsterWebApp.Beans.Salarie;

@Controller
public class LoginController {

	public static final String ATT_SESSION_USER = "sessionUtilisateur";
	public static final String ATT_MSG_WARNING = "Warning";
	public static final String ATT_MSG_SUCCESS = "Success";

	@Autowired
	private Authentication authentication;

	@GetMapping("/connexion")
	public String connexionGET() {
		return "authentification/login";
	}

	@PostMapping("/connexion")
	public String ConnexionPOST(HttpSession session, WebRequest request, Model model) {
		String type = request.getParameter("Type");
		String Id = request.getParameter("Identifiant");
		String Password = request.getParameter("Password");

		if (type.equals("Utilisateur")) {
			EsterUser esterUser;
			if ((esterUser = authentication.authenticateEsterUser(Id,Password))!=null) {
				session.setAttribute(ATT_SESSION_USER, esterUser);
				return "redirect:/utilisateur";
			}else {
				model.addAttribute(ATT_MSG_WARNING, " Votre identifiant ou votre mot de passe est incorrect.");
			}
		}
		
		if (type.equals("Salarie")) {
			Salarie salarie;
			if ((salarie = authentication.authenticateSalarie(Id))!=null) {
				session.setAttribute(ATT_SESSION_USER, salarie);
				return "redirect:/salarie";
			}else {
				model.addAttribute(ATT_MSG_WARNING, " Votre identifiant est incorrect.");
			}
		}
		
		if (type.equals("Entreprise")) {
			Entreprise entreprise;
			if ((entreprise = authentication.authenticateEntreprise(Id,Password))!=null) {
				session.setAttribute(ATT_SESSION_USER, entreprise);
				return "redirect:/entreprise";
			}else {
				model.addAttribute(ATT_MSG_WARNING, " Votre identifiant est incorrect.");
			}
		}
		
		return "authentification/login";
	}
}
