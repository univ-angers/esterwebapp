package fr.univangers.ester.EsterWebApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class EsterWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsterWebAppApplication.class, args);
	}
}