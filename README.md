# EsterWebApp
<h1>Projet d'application web pour le compte d'ESTER.</h1>

- <b>Technos :</b>
    * Backend : 
        * Java SE 11
        * Spring Boot 2.1.2
        * Maven :
            * groupId : fr.univ-angers.ester
	        * artifactId : EsterWebApp
            * version : 0.0.1-SNAPSHOT
	        * name EsterWebApp
	        * Packaging : JAR
	    * Accès BDD (DAO) : Spring data MongoDB
    * Serveur d'application : Apache Tomcat v9.0.14 embarqué via Spring Boot
    * IDE : Eclipse via STS 4 version 4.1.0-RELEASE (Spring Tool Suite)
    * BDD : MongoDB (db version v4.0.4)
    * Client MongoDB : Robo 3T v1.2.1

- Les ressources documentaires du projet se trouvent dans le dossier *Livrables/Ressources Documentaires*. 
    * Parmi les documents, se trouve le manuel de l'administrateur/développeur
- Un Dump de la base de données est disponible en ZIP dans le dossier *Livrables/Dump BDD*
- Le jeu de données COSALI est présent dans le dossier *Livrables/Jeu de données*
- L'archive contenant les sources de la première version développée par les M1 se trouve dans le dossier *Livrables/Projet_ESTER_Souces(1èreVersionM1)*
    * Les sources de cet ancien projet traitant plus particulièrement des graphiques avec HighCharts seront d'une très grande aide pour la suite : 
        * Projet_ESTER/src/fr/univangers/ester/beans/Resultat.java
        * Projet_ESTER/src/fr/univangers/ester/servlet/Resultat.java
        * Projet_ESTER/WebContent/js/resultat.js
        * Projet_ESTER/WebContent/WEB-INF/jsp/Resultat.jsp
